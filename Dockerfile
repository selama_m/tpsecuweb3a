# Copyright {2017} {Viardot Sebastien}
# Pour créer l'image docker build . -t tpsecuweb
# Image de base
FROM debian:stretch
# Auteur
MAINTAINER Sebastien Viardot <Sebastien.Viardot@grenoble-inp.fr>
# Installe le nécessaire pour disposer du framework play pour le TP
RUN apt-get update && \
    apt-get -y install openjdk-8-jdk wget python ant git unzip &&\
    cd /tmp &&\
    wget https://downloads.typesafe.com/play/1.5.3/play-1.5.3.zip &&\
    unzip play-1.5.3.zip && mv play-1.5.3 /opt/play
VOLUME /source
EXPOSE 9000
WORKDIR /source
